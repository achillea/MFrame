#ifndef __MUTEX__
#define __MUTEX__


#include <Windows.h>

class MMutex
{

protected:
	CRITICAL_SECTION	m_csObj;			// for windows

public:

	MMutex()
	{
		InitializeCriticalSection( &m_csObj );
	}

	virtual ~MMutex()
	{
		DeleteCriticalSection( &m_csObj );
	}

	// 锁定
	void Lock()
	{
		EnterCriticalSection( &m_csObj );
	}

	// 解锁
	void UnLock()
	{
		LeaveCriticalSection( &m_csObj );
	}
};



class MMutexLock
{
protected:
	MMutex		*m_pMutex;

public:
	MMutexLock( MMutex &mutex )
	{
		m_pMutex	= &mutex;
		m_pMutex->Lock();
	}

	virtual ~MMutexLock()
	{
		m_pMutex->UnLock();
	}

};


class MFlagLock
{
protected:
	bool	m_bFlag;

public:

	MFlagLock(){ Reset(); }

	// 试图锁定
	bool TryLock()
	{
		if ( !m_bFlag )
		{
			m_bFlag = true;
			return true;
		}

		return false;
	}

	// 解锁
	void UnLock(){ m_bFlag = false; }

	// 重置为未锁定状态
	void Reset(){ UnLock(); }

	// 是否已经锁定
	bool IsLock(){ return m_bFlag; }


};






#endif