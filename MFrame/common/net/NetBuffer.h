/********		网络层缓冲区		********/

#ifndef __NETBUFFER__
#define __NETBUFFER__

#include <vector>


// 封包大小限制
#define PACKET_SIZE_LIMIT 1024*1024


class NetBuff : public std::vector< char >
{

protected:

	size_t		m_uiHead;			// 头位置
	size_t		m_uiTail;			// 尾位置

public:

	NetBuff()
	{
		m_uiHead	= m_uiTail	= 0;
	}

	virtual void MoveHead( size_t uiChgSize )
	{
		m_uiHead += uiChgSize;
	}



	virtual void Reset() { m_uiHead = m_uiTail = 0; }

};


class NetRecvBuff : public NetBuff
{

public:

	NetRecvBuff(){}

public:

	void MoveTail( size_t uiChgSize )
	{
		m_uiTail += uiChgSize;
	}

	// 移动剩余数据到缓冲的开始位置
	void MoveDataToHead();

	// 获取可用的接收缓冲区域
	void GetUsableRecvArea( char* &ptr, size_t &uiSize );

	// 获取已经接收的数据区域
	bool GetRecvDataArea( char* &ptr, size_t &uiSize );
};


class NetSendBuff : public NetBuff
{
public:
	NetSendBuff() {}


public:

	// 判断是否实际数据为空
	bool IsEmpty() { return m_uiHead == m_uiTail; }

	// 往缓冲区中压入数据
	void PushData( const char *pData, size_t uiDataSize );

	// 获取待发送的数据
	bool GetSendDataArea( char* &ptr, size_t &uiSize );



};

#endif 