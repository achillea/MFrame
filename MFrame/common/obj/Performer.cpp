#include "Performer.h"

Performer::Performer()
{
}

Performer::~Performer()
{
	if ( !m_queEvent.empty() )
	{
		// 消息没有处理完
	}
}


void Performer::Tick()
{
	while ( true )
	{
		EventExPtr pEvent = GetEvent();

		if ( pEvent == 0 )
			continue;

		ProcessEvent(pEvent);
	}
}


void Performer::QueueingEvent( EventExPtr pEvent )
{
 	m_queEvent.push( pEvent );
}


EventExPtr Performer::GetEvent()
{
	if ( m_queEvent.empty() )
		return 0;

	EventExPtr pEvent = m_queEvent.front();
	m_queEvent.pop();

	return pEvent;
}










