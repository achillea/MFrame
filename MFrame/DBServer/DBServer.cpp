// DBServer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "DBModule\ConnFactory.h"
#include "DBHouse\DBConfig.h"




int _tmain(int argc, _TCHAR* argv[])
{



	DBConfig cfg;
	DBConfig::DBCfg* pCfg = cfg.GetConfig();

	mysqlpp::Connection *pConn = ConnFactory::GetConnection( pCfg->strDBName, pCfg->strServer.c_str(), pCfg->strUserName, pCfg->strPassword, pCfg->iPort, pCfg->strCharset);
	if ( pConn == NULL )
		return 1;
	



	{
		char cquery[256] = {0};
		char header[256] = "call pro_test(%d,'%s')";
		int id = 1000100;
		char param[128] = "在线5分钟";
		sprintf( cquery, header, id, param );
		mysqlpp::Query query = pConn->query(cquery);

		mysqlpp::StoreQueryResult res = query.store();
		for ( size_t i = 0; i < res.num_rows(); i++ )
		{
			int ilower = res[i]["level_lower"];
			int ifixed = res[i]["fixed"];
			std::cout << "-----\n" <<  res[i]["name"] << res[i]["level_lower"] << res[i]["fixed"] << std::endl;
		}

		for ( int i = 1; query.more_results(); i++ )
		{
			res = query.store_next();
			for ( size_t i = 0; i < res.num_rows(); i++ )
			{
				int ilower = res[i]["level_lower"];
				int ifixed = res[i]["fixed"];
				std::cout << "-----\n" <<  res[i]["name"] << res[i]["level_lower"] << res[i]["fixed"] << std::endl;
			}
		}
	}


	{
//		query.reset();
//		query.exec("CALL pro_test(1000101)");
		mysqlpp::Query query = pConn->query("CALL pro_test(1000100,'在线6分钟')");
		mysqlpp::StoreQueryResult res1 = query.store();
		for ( size_t i = 0; i < res1.num_rows(); i++ )
		{
			int ilower = res1[i]["level_lower"];
			int ifixed = res1[i]["fixed"];
			std::cout << "-----\n" <<  res1[i]["name"] << res1[i]["level_lower"] << res1[i]["fixed"] << std::endl;
		}
	}
	


	getchar();

	return 0;
}

