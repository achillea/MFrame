#include "ConnFactory.h"


mysqlpp::Connection* ConnFactory::GetConnection( const char* db, const char* server,
			const char* user, const char* password,
			unsigned int port, const char* charset)
{

	mysqlpp::Connection* con = new mysqlpp::Connection();
	con->set_option( new mysqlpp::MultiStatementsOption(true) );
	con->set_option( new mysqlpp::SetCharsetNameOption(charset) );
	if ( !con->connect( db, server, user, password, port) )
	{
		return NULL;
	}

	return con;
}