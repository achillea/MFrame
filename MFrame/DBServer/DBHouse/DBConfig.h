#include <string>

class DBConfig
{
public:
	struct DBCfg
	{
//		std::string		strDBName;
		char	strDBName[128];
		std::string strServer;
		int		iPort;
		char	strUserName[128];
		char	strPassword[128];
		char	strCharset[128];
	};


	DBConfig(){ LoadConfig( "script/config.lua" ); };
	virtual ~DBConfig(){};

	bool LoadConfig( const char *filename );

	DBCfg* GetConfig() { return &m_stConfig; }


protected:

	DBCfg	m_stConfig;

};