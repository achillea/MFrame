#include "userManager.h"


UserManager::UserManager()
{
}

UserManager::~UserManager()
{
}

bool UserManager::AddUser( UidType UID )
{
	User* pUser = 0;
	pUser = new User( UID );
	if ( pUser == 0 )
	{
		// 错误日志，创建user失败

		return false;
	}


	m_mapUsers[UID] = pUser;


	// 后续的数据填充
	// ..


	return true;

}

bool UserManager::DelUser( UidType UID )
{
	std::map< UidType, User* >::iterator it = m_mapUsers.find( UID );
	if ( it == m_mapUsers.end() )
	{
		// 错误日志， 没有此user

		return false;
	}

	// 安全删除user指针
	SAFE_DELETE( it->second );

	// 从列表中释放
	m_mapUsers.erase( it );

	return true;
}


User* UserManager::GetUser( UidType UID )
{
	std::map< UidType, User* >::iterator it = m_mapUsers.find( UID );
	if ( it == m_mapUsers.end() )
	{
		// 错误日志， 没有此user

		return 0;
	}

	return it->second;

}



