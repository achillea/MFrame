#include "SimObject.h"


SimObject::SimObject() : m_iUID(0)
{
	wchar_t buff1[128] = {0};
	wchar_t buff2[128] = {0};
	wchar_t buff3[128] = {0};

	static unsigned __int64 sSeedNum = 0;

	swprintf( buff3, L"M_%s_%s_%020d", _wstrdate(buff1), _wstrtime(buff2), sSeedNum++ );

	m_wstrName = buff3;
}

SimObject::~SimObject()
{
}

