#ifndef __PERFORMER__
#define __PERFORMER__


#include "SimObject.h"
#include "Event.h"
#include "queue"

#include "EventEx.h"

SmartPointer(EventEx);
class Performer : public SimObject
{

public:
 	Performer();
	virtual ~Performer();

	
	void QueueingEvent( EventExPtr pEvent );
	unsigned int GetQueueSize()
	{
		return m_queEvent.size();
	}



protected:

	virtual void Tick();

	EventExPtr GetEvent();


protected:
	virtual void ProcessEvent( EventExPtr pEvent ) = 0;


protected:

//	std::queue<BaseEvent*> 	m_queEvent;
	std::queue<EventExPtr>	m_queEvent;

};


#endif
