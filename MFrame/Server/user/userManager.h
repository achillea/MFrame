#ifndef __USERMANAGER__
#define __USERMANAGER__


#include "user.h"

class UserManager : public Performer, public MSingleton< UserManager >
{
public:
	UserManager();
	virtual ~UserManager();


	// 添加用户
	// UID	：用户的UID
	bool	AddUser( UidType UID );	

	// 删除用户
	// UID	: 用户UID
	bool	DelUser( UidType UID );

	// 获取User指针
	User*	GetUser( UidType UID );



protected:

	std::map< UidType, User* >	m_mapUsers;				// first : User UID		second : User Point

};


#endif