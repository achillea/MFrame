#ifndef __LTM_UNUSEDID__
#define __LTM_UNUSEDID__

#include <queue>
#include "MMutex.h"

class UnusedID
{
public:
	UnusedID( int iMaxID );
	virtual ~UnusedID();

	// 获取一个未使用的ID
	int GetUnusedID();

	// 归还一个ID
	void GiveBackID( int iID );

protected:

	std::queue< int >	_queID;	
	MMutex				_mutexID;

};


#endif