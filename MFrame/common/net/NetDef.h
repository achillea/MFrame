#ifndef __NETDEF__
#define __NETDEF__

#include <Windows.h>
#include <string>


#define CLOSE_SOCKET(socket) \
	if ( socket != INVALID_SOCKET ) \
	{ \
	::shutdown( socket, SD_BOTH ); \
	::closesocket( socket ); \
	socket = INVALID_SOCKET; \
	}



#endif