#include "EventRoute.h"


EventRoute::EventRoute()
{

	// 初始化事件执行
	m_EventProc.InitOnEventFuncArray();

}


EventRoute::~EventRoute()
{

}



void EventRoute::ProcessEvent( EventExPtr Event )
{
	BaseEvent* pBaseEvent = reinterpret_cast<BaseEvent*>(Event->_buff);
	if ( pBaseEvent == NULL )
	{
		// 失败，加入日志
		return;
	}

	m_EventProc.OnDispatch(*pBaseEvent);
}


// 线程的真实处理函数
unsigned long EventRoute::ThreadProcReal()
{
	Performer::Tick();
	return 0;
}