#ifndef __MMUTEX__
#define __MMUTEX__


#include <Windows.h>

class MMutex
{

protected:
	CRITICAL_SECTION	m_csObj;			// for windows

public:

	MMutex()
	{
		InitializeCriticalSection( &m_csObj );
	}

	virtual ~MMutex()
	{
		DeleteCriticalSection( &m_csObj );
	}

	// 锁定
	void Lock()
	{
		EnterCriticalSection( &m_csObj );
	}

	// 解锁
	void UnLock()
	{
		LeaveCriticalSection( &m_csObj );
	}
};



class MMutexLock
{
protected:
	MMutex		*m_pMutex;

public:
	MMutexLock( MMutex &mutex )
	{
		m_pMutex	= &mutex;
		m_pMutex->Lock();
	}

	virtual ~MMutexLock()
	{
		m_pMutex->UnLock();
	}

};


class MFlagLock
{
protected:
	bool	m_bFlag;

public:

	MFlagLock(){ Reset(); }

	// 试图锁定
	bool TryLock()
	{
		if ( !m_bFlag )
		{
			m_bFlag = true;
			return true;
		}

		return false;
	}

	// 解锁
	void UnLock(){ m_bFlag = false; }

	// 重置为未锁定状态
	void Reset(){ UnLock(); }

	// 是否已经锁定
	bool IsLock(){ return m_bFlag; }


};



// helper class for lock
class MLocker
{
public:

	MLocker( CRITICAL_SECTION& cs ) : m_pcs( &cs )
	{
		Lock();
	}

	~MLocker()
	{
		if ( m_pcs )
			Unlock();
	}

	void Lock()
	{
		EnterCriticalSection( m_pcs );
	}

	void Unlock()
	{
		LeaveCriticalSection( m_pcs );
		m_pcs = NULL;
	}


protected:

	CRITICAL_SECTION*	m_pcs;

};






#endif