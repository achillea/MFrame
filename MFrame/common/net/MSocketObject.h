#ifndef __MSOCKETOBJECT__
#define __MSOCKETOBJECT__


#include <WinSock2.h>
#include <vector>
#include "Overlapped.h"
#include "NetBuffer.h"

class MSocketObject
{

public:

//	MSocketObject( const MSocketObject& right );
//	MSocketObject& operator=( const MSocketObject& right );

	MSocketObject(void);
	virtual ~MSocketObject(void);


public:
	/*
	void CloseSocket();
	bool Connect( const char* szIP = NULL, unsigned short usPort = 0, bool bUseIocp = true, std::vector<std::pair<int, int>>* vecOpt = NULL );

	bool SendData( const char* szData, int iSize );
	bool InitRecv();

	void OnIOCompleted( MOVERLAPPED::IO_MODE eMode );
	void OnSendCompleted( unsigned long ulTransfered );

	void SetRemoteIP( const char* szIP ) { m_sockaddr.sin_addr.s_addr = inet_addr( szIP ); }
	void SetRemotePort( unsigned short usPort ) { m_sockaddr.sin_port = htons( usPort ); }
	void SetSocketInfo( SOCKET socket, const SOCKADDR_IN& sockAddr ) { m_sock = socket; m_sockaddr = sockAddr; }


	virtual void OnRecvCompleted( unsigned long ulTransfered ) = 0;
	virtual void OnSocketError();
	virtual void OnAcceptConnection() {}


	bool IsConnected() const { return m_sock != INVALID_SOCKET; }
	bool IsSending() const { return m_ovlSend.m_bSending; }
	void SetExecKey( unsigned long ulKey ) { m_ulExecKey = ulKey; }
	unsigned long GetExecKey() const { return m_ulExecKey; }
	void SetIOEvent( MOVERLAPPED::IO_MODE eMode, HANDLE hEvent );
	bool CancelIO()	{ return ::CancelIo( (HANDLE)m_sock ); }


	unsigned int GetIP() const { return m_sockaddr.sin_addr.S_un.S_addr; }
	const char* GetIPStr() const { return inet_ntoa( m_sockaddr.sin_addr ); }
	unsigned short GetPort() const { return ntohs( m_sockaddr.sin_port ); }


	static void DumpSendQueueLog( std::wostream &stm ){}

	int GetDisconnectReason() { return m_iDisconnectReason; }
	void SetDisconnectReason( int iDisconnectReason );

	// 初始化接收缓存大小（也可不初始化）
	void InitRecvBuffSize( int iSize ) { m_xRecvBuff.resize(iSize); }

	

	*/



protected:

	unsigned long		m_ulExecKey;
	SOCKADDR_IN			m_sockaddr;
	MOVERLAPPED			m_ovlRecv;
	MOVERLAPPED			m_ovlSend;
	SOCKET				m_sock;
	CRITICAL_SECTION	m_csSock;

	int					m_iDisconnectReason;

	NetRecvBuff			m_xRecvBuff;

	NetSendBuff			m_arySendBuff[2];			// 等待发送缓冲交换列
	int					m_iWaitSndIndx;				// 等待发送的缓冲序列索引
	int					m_iCurrSndIndx;				// 正在发送的缓冲序列索引



};



#endif