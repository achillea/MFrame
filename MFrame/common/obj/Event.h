#ifndef __EVENT__
#define __EVENT__


#include <string.h>
struct BaseEvent
{
	BaseEvent()
	{
		EventID = 0;

		memset( buf, 0, sizeof(buf) );
	}

	char 	EventID; 			// 消息ID
		
	char	buf[128];			// 测试字符串，char型的EventID避免网络消息的字节对齐导致的数据传输不全，后期再做网络消息序列化
};


// 定义消息ID集合
#define DECLARE_EVENT_MAP enum EVENT_MAP{ EGS_EVENT_DECLARE_BEGIN,
#define DECLARE_EVENT_MAP_END EGS_EVENT_DECLARE_END, };



// 定义消息数量
#define EVENT_SENTINEL EGS_EVENT_DECLARE_END



#define DECL_EVENT(EVENT_ID) struct E##EVENT_ID : public BaseEvent
#define INIT_EVENT(EVENT_ID) E##EVENT_ID(){ BaseEvent::EventID = EVENT_ID; }


// 将BaseEvent转化为事件ID对应的数据结构
#define HWG_EVENT(EVENT_ID) E##EVENT_ID KEvent;							\
			KEvent = *(static_cast<E##EVENT_ID *>(&Msg));						\



#endif
