#include "NetBuffer.h"


void NetRecvBuff::MoveDataToHead()
{
	if ( m_uiHead > 0 )
	{
		if ( m_uiHead == m_uiTail )
		{
			m_uiHead = m_uiTail = 0;
		}
		else
		{
			memmove( &(*this)[0], &(*this)[m_uiHead], m_uiTail - m_uiHead );
		}
	}
}

void NetRecvBuff::GetUsableRecvArea( char* &ptr, size_t &uiSize )
{

	// 接收缓冲增量
#define NBR_INCREASE 8*1024

	// 若缓冲不够，增大
	if ( m_uiTail >= size() )
	{
		resize ( size() + NBR_INCREASE );
	}

	// 可用区域在尾位置
	ptr = &(*this)[m_uiTail];
	uiSize = size() - m_uiTail;
}

// 获取已接收的数据区域
bool NetRecvBuff::GetRecvDataArea( char* &ptr, size_t &uiSize )
{
	if ( m_uiTail > m_uiHead )
	{
		// 从头位置开始
		ptr = &(*this)[m_uiHead];
		uiSize = m_uiTail - m_uiHead;

		return true;
	}

	return false;

}



// 往缓冲区中压入数据
void NetSendBuff::PushData( const char *pData, size_t uiDataSize )
{
	size_t uiTotalSize = uiDataSize + sizeof( int );		// 还需要加4个字节存放总大小

	// 若缓冲不够，则增大
	if ( m_uiTail + uiTotalSize > size() )
	{
		resize ( m_uiTail + uiTotalSize );
	}

	// 从尾位置添加
	char *ptr = &(*this)[m_uiTail];

	// 写入总得大小
	*((int*)ptr) = uiTotalSize;
	ptr += sizeof (int);

	// 写入数据
	memcpy( ptr, pData, uiDataSize );

	m_uiTail += uiTotalSize;

}

// 获取待发送的数据区域
bool NetSendBuff::GetSendDataArea( char* &ptr, size_t &uiSize )
{
	if ( m_uiTail > m_uiHead )
	{
		// 取头部位置
		ptr  = &(*this)[m_uiHead];
		uiSize = m_uiTail - m_uiHead;

		return true;
	}

	return false;
}








