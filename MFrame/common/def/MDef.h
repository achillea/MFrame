#ifndef __MDEF__
#define __MDEF__


#include <time.h>
#include <math.h>




#include <list>
#include <vector>
#include <stack>
#include <map>
#include <queue>
#include <string>
#include <algorithm>


#include "boost/shared_ptr.hpp"



#define WSABUFF_LEN_LMT 8 * 1024			// 网络数据传输长度限制

#define UidType unsigned __int64

#define SmartPointer(_Class_Name) class _Class_Name; typedef boost::shared_ptr<_Class_Name> _Class_Name##Ptr


#ifndef SAFE_DELETE
#define SAFE_DELETE(p)	{ if(p){ delete p; p = 0; } }
#endif


#ifndef SAFE_CLOSE_HANDLE
#define SAFE_CLOSE_HANDLE(p)	{ if(p){CloseHandle(p); p = 0; } }
#endif


#ifndef ARRAY_COUNT
#define ARRAY_COUNT(a)	(sizeof(a) / sizeof(a[0]))
#endif



#endif