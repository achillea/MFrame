#include "MThreadBase.h"


MThreadBase::MThreadBase()
{
	m_hThread		= 0;
	m_ulThreadID	= 0;
	m_pData			= 0;
	m_ulProcessorID	= 0;
	m_bExitSignal	= false;

}

MThreadBase::~MThreadBase()
{
	SAFE_CLOSE_HANDLE( m_hThread );
}


bool MThreadBase::InitThread( void *pData )
{
	// 保存自定义数据
	m_pData = pData;

	// 自定义初始化操作
	if ( !CustomInitBeforeThread() )
		return false;

	// 创建线程
	m_hThread	= CreateThread( 0, 0, _ThreadProcCallBack, this, 0, &m_ulThreadID );

	m_ulProcessorID		= AdjustProcessor( m_hThread, true );

	return m_hThread != NULL;
}

unsigned long MThreadBase::WaitForThreadEnd( unsigned long ulWaitMillisecond )
{
	return m_hThread ? WaitForSingleObject( m_hThread, ulWaitMillisecond ) : 0;
}

void MThreadBase::SafeExitThread( unsigned long ulExitCode )
{
	if ( m_hThread )
		::ExitThread( ulExitCode );
}

void MThreadBase::ForceEndThread()
{
	if ( m_hThread )
		::TerminateThread( m_hThread, 0 );
}

unsigned long MThreadBase::SetProcessorID( unsigned long ulProcessorID )
{
	m_ulProcessorID		= AdjustProcessor( m_hThread, false, ulProcessorID );

	return m_ulProcessorID;
}

unsigned long MThreadBase::AdjustProcessor( HANDLE hThread, bool bAuto, unsigned long ulSpecID )
{
	static MMutex			s_mutex;
	static bool				s_bInit = false;
	static unsigned long	s_ulProcessorNum	= 1;
	static unsigned long	s_ulProcessorID		= 0;
	static unsigned long	s_ulMarkAry[] = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768 };

	MMutexLock kLock( s_mutex );

	unsigned long ulRet;


	if ( !s_bInit )
	{
		srand( time(0) );

		SYSTEM_INFO xInfo;
		GetSystemInfo( &xInfo );
		s_ulProcessorNum = xInfo.dwNumberOfProcessors;
		if ( s_ulProcessorNum > 0 )
			s_ulProcessorID = rand() % s_ulProcessorNum;

		// 此处可以插入日志

		unsigned long ulVal = 0;
		for ( unsigned long k = 0; k < s_ulProcessorNum && k < ARRAY_COUNT(s_ulMarkAry); k++ )
		{
			ulVal |= s_ulMarkAry[k];
		}

		HANDLE hHandle = GetCurrentProcess();
		ulRet = SetProcessAffinityMask( hHandle, ulVal );
		if ( ulRet == 0 )
		{
			// 插入错误日志
		}

		s_bInit = true;

	}
	

	if ( !hThread )
		return 0;

	unsigned long ulProccID;

	// 自动
	if ( bAuto )
	{
		ulProccID = s_ulProcessorID;

		ulRet = SetThreadIdealProcessor( hThread, ulProccID );
		if ( ulRet == (unsigned long)-1 )
		{
			// 插入错误日志 SetThreadIdealProcessor failed.
		}

		ulRet = SetThreadAffinityMask( hThread, s_ulMarkAry[ulProccID] );
		if ( ulRet == 0 )
		{
			// 插入错误日志 SetThreadAffinityMask failed.
		}

		s_ulProcessorID++;
		if ( s_ulProcessorID >= s_ulProcessorNum )
			s_ulProcessorID = 0;

	}
	// 指定
	else
	{
		ulProccID = ulSpecID;

		if ( ulProccID >= s_ulProcessorNum )
			ulProccID = 0;


		ulRet = SetThreadIdealProcessor( hThread, ulProccID );
		if ( ulRet == (unsigned long)-1 )
		{
			// 插入错误日志 SetThreadIdealProcessor failed.
		}

		ulRet = SetThreadAffinityMask( hThread, s_ulMarkAry[ulProccID] );
		if ( ulRet == 0 )
		{
			// 插入错误日志 SetThreadAffinityMask failed.
		}


	}


	return ulProccID;

}


unsigned long WINAPI MThreadBase::_ThreadProcCallBack( void *pData )
{
	if ( pData == NULL )
		return 0;

	MThreadBase* pThreadBase = (MThreadBase*)pData;

	return pThreadBase->ThreadProcReal();
}


















