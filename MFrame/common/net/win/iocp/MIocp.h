#ifndef __LTM_MIOCP__
#define __LTM_MIOCP__


#include <map>
#include <queue>
#include <vector>
#include <WinSock2.h>


#include "def/MDef.h"
#include "def/UnusedID.h"




#define CLOSE_SOCKET(socket)		\
			if (socket != INVALID_SOCKET)	\
			{								\
			::shutdown(socket, SD_BOTH);	\
			::closesocket( socket );		\
			socket = INVALID_SOCKET;		\
			}								


#define EXEC_ID	DWORD





typedef void (RecvCallBack) (int, char*, int);


class MIocp
{

public:
	
	struct MOVERLAPPED
	{
	public:
		enum IO_MODE
		{
			IO_RECV,
			IO_SEND,
		};

		MOVERLAPPED()
		{
			Clear();
		}

		void Clear()
		{
			m_ol.hEvent = NULL;
			m_ol.Internal	= 0;
			m_ol.InternalHigh	= 0;
			m_ol.Offset		= 0;
			m_ol.OffsetHigh		= 0;
		}


	public:
		OVERLAPPED m_ol;

		// 用于WSARecv/WSASend的buffer
		WSABUF		m_wsaBuffer;

		char		m_cBuffer[WSABUFF_LEN_LMT];

		// 读写状态
		IO_MODE		m_eMode;

	};

	struct SocketObject
	{

	public:
		SocketObject()
		{
			_socket = 0;

			_id = 0;
		}

	public:
		EXEC_ID		_id;
		SOCKET		_socket;
	};


public:
	MIocp(void);
	virtual ~MIocp(void);

	
	// 开始
	// iMaxClient		:最大连接数
	// iThreadNum		:工作线程数
	// usPort			:端口
	bool Start( int iMaxClient, int iThreadNum, unsigned short usPort, RecvCallBack fnRecvCB );



	// 发送一个网络数据包
	bool SendData( std::vector<EXEC_ID> vecId, char* pBuf, int iLen );


private:

	HANDLE GetCompletionPort(){ return m_hCompletionPort; }

	SocketObject* GetSocketObject(EXEC_ID id){ return m_mapSockObj[id]; }

protected:

	// 线程入口调用函数
	static DWORD WINAPI _ThreadProcCB( void *pData );

	// 接收到消息时的逻辑
	bool	OnRecvCompleted(EXEC_ID id, MOVERLAPPED *pOL);

private:

	std::map< EXEC_ID, SocketObject* >		m_mapSockObj;	// 连接的套接字 first exeid		second SocketObject

	std::map< DWORD, HANDLE >		m_mapThread;			// 线程列表 first Thread ID		second Thread Handle

	SOCKET							m_sock;					// 监听socket

	HANDLE							m_hCompletionPort;		// 完成端口

	UnusedID*						m_pUnusedID;			// ID控制器

	RecvCallBack*					m_pfnRecvCB;				// 接收到网络消息时的回调函数

};

#endif
