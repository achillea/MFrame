#include "DBConfig.h"
#include "sh.h"

bool DBConfig::LoadConfig( const char *filename )
{
	LuaTable lt;
	lt.DoFile( filename );

	// 开始读取数据
	if ( lt.GetValue( "port", m_stConfig.iPort ) == false )
	{
		// 加入日志

		return false;
	}

	if ( lt.GetValue("db", m_stConfig.strDBName) == false )
	{
		// 加入日志

		return false;
	}

	if ( lt.GetValue("server", m_stConfig.strServer) == false )
	{
		// 加入日志

		return false;
	}

	if ( lt.GetValue("username", m_stConfig.strUserName) == false )
	{
		// 加入日志

		return false;
	}

	if ( lt.GetValue("password", m_stConfig.strPassword) == false )
	{
		// 加入日志

		return false;
	}

	if ( lt.GetValue("charset", m_stConfig.strCharset) == false )
	{
		// 加入日志

		return false;
	}



	return true;
}