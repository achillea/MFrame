/**************************************************************
					事件路由
			1、接受事件放入事件队列				
			2、按事件队列顺序依次进行事件处理
			3、单独占用一个线程
***************************************************************/


#ifndef __EVENTROUTE__
#define __EVENTROUTE__

//#include "obj/Performer.h"
//#include "thread/MThreadBase.h"
#include "EventProc.h"

class EventRoute : public Performer, public MThreadBase
{

public:
	EventRoute();
	virtual ~EventRoute();


protected:
	virtual void ProcessEvent( EventExPtr Event );

	virtual unsigned long ThreadProcReal();

protected:
	EventProc m_EventProc;

};



#endif