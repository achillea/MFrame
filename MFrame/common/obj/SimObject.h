#ifndef __SIMOBJECT__
#define __SIMOBJECT__


#include <string>


class SimObject
{
	public:
		SimObject();
		virtual ~SimObject();


		const std::wstring &GetName() const { return m_wstrName; }
		void SetName(const std::wstring &wstrName) { m_wstrName = wstrName; }
		void SetName( const wchar_t* szName ) { m_wstrName = szName; }


		__int64 GetUID() const { return m_iUID; }
		void SetUID( __int64 iUID ) { m_iUID = iUID; }




	protected:
		std::wstring 	m_wstrName;
		__int64 	m_iUID;

};


#endif
