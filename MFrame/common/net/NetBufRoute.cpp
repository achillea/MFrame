#include "NetBufRoute.h"

#include "obj/EventEx.h"


EventRoute			g_EventRoute;



NetBufRoute::NetBufRoute()
{
	g_EventRoute.InitThread(&g_EventRoute);
}

NetBufRoute::~NetBufRoute()
{
}

void NetBufRoute::QueueBuf(int id, char* pBuf, int len)
{

	if ( pBuf == NULL || id <= 0 || len <= 0 )
	{
		// 失败，加入日志
		return;
	}

	printf("recv : %-5d , %15s, %-2d\n", id, pBuf, len);

	

	boost::shared_ptr< EventEx > pEvent(new EventEx(id, pBuf, len));
	if ( pEvent == NULL )
	{
		// 失败，加入日志
		return;
	}

	g_EventRoute.QueueingEvent(pEvent);

}
