#ifndef __EVENTFUNC__
#define __EVENTFUNC__

#include "Event.h"

template< typename T, int EventIDSize >
class EventFunc
{
	typedef void (T::*FPHWGONEVENT) ( BaseEvent& );
protected:

	FPHWGONEVENT m_fpHwgOnEvent[EventIDSize+1];			// 事件函数指针集合

public:

	// 初始化操作，需要重载
	virtual void InitOnEventFuncArray() = 0;

public:
	EventFunc(){};
	virtual ~EventFunc(){};


	// 调用事件对应函数处理
	bool OnDispatch( BaseEvent& msg )
	{
		(((T*)this)->*m_fpHwgOnEvent[msg.EventID])(msg);

		return true;
	}


#define DECL_ON_EVENT_FUNC(EventID) void ON_##EventID( BaseEvent& Msg);
#define REG_EVENT_FUNC(_Class_Name,EventID) m_fpHwgOnEvent[EventID] = &_Class_Name::ON_##EventID;
#define IMPL_EVENT_FUNC(_Class_Name,EventID) void _Class_Name::ON_##EventID( BaseEvent& Msg )

};



#endif