#ifndef __MSINGLETON__
#define __MSINGLETON__

/*************************		��ʵ������			*************************/

template< typename T >
class MSingleton
{
    
public:
    static T* GetInstance()
    {
      
        if ( m_pInstance == NULL )
        {
            m_pInstance = new T();
        }
        
        return m_pInstance;
    };
    
protected:
    MSingleton(){};
    virtual ~MSingleton(){ if( m_pInstance != NULL ){ delete m_pInstance; };  m_pInstance=NULL; };
    
    static T* m_pInstance;
    


};

template< typename T > T* MSingleton<T>::m_pInstance = NULL;

#endif