#include "EventEx.h"

#include <string.h>

EventEx::EventEx(int id, char* pBuf, int len)
{
	_ExeID	= id;
	memset( _buff, '\0', sizeof(_buff) );
	memcpy( _buff, pBuf, len );
}

EventEx::~EventEx()
{
}